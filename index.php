<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Online Calculator</title>
<link href="style.css" rel="stylesheet">
</head>
<body>
<div id="Web_1366__1">
	<svg class="Rectangle_1">
		<rect id="Rectangle_1" rx="0" ry="0" x="0" y="0" width="369" height="531">
		</rect>
	</svg>
	<div id="Online_Calculator">
		<span>Online Calculator</span>
	</div>
	<div id="INPUT_BILANGAN">
		<span>INPUT BILANGAN</span>
	</div>
	<svg class="Line_1" viewBox="0 0 369 1">
		<path id="Line_1" d="M 0 1 L 369 0">
		</path>
	</svg>
	<form method="post" action="">
		<input type="text" name="bilangan1" placeholder="Bilangan pertama" class="Rectangle_2"><br>
		<input type="text" name="bilangan2" placeholder="Bilangan Kedua" class="Rectangle_3b">
		<input type="submit" value="Hitung" name="hitung bilangan 1 dan 2" class="Rectangle_4">
	</form>
                <svg class="Rectangle_6">
                		<rect id="Rectangle_6" rx="0" ry="0" x="0" y="0" width="369" height="325">
                		</rect>
                	</svg>
                	            	<svg class="Rectangle_7">
                                		<rect id="Rectangle_7" rx="0" ry="0" x="0" y="0" width="368" height="43">
                                		</rect>
                                	</svg>
                                	<div id="HASIL">
                                		<span>HASIL</span>
                                	</div>
	<?php
			if($_SERVER['REQUEST_METHOD'] == 'POST'){
				$bilangan1 = $_POST['bilangan1']; //memasukkan value dari inputan bilangan1 ke variabel $bilangan1
				$bilangan2 = $_POST['bilangan2']; //memasukkan value dari inputan bilangan2 ke variabel $bilangan2

				/*
					* @desc method penjumlahan digunakan untuk menjumlahkan bilangan1 dan bilangan2
					* @param $bilangan1 berisi nilai dari bilangan ke 1 atau operand ke 1
					* @param $bilangan2 berisi nilai dari bilangan ke 2 atau operand ke 2
					* @return berupa $bilangan1+$bilangan2 yaitu penjumlahan $bilangan 1 dan $bilangan 2
				*/
				function penjumlahan($bilangan1, $bilangan2){
					return $bilangan1+$bilangan2;
				}

				/*
					* @desc method pengurangan digunakan untuk mengurangkan bilangan1 dan bilangan2
					* @param $bilangan1 berisi nilai dari bilangan ke 1 atau operand ke 1
					* @param $bilangan2 berisi nilai dari bilangan ke 2 atau operand ke 2
					* @return berupa $bilangan1-$bilangan2 yaitu pengurangan $bilangan 1 dan $bilangan 2
				*/
				function pengurangan($bilangan1, $bilangan2){
					return $bilangan1-$bilangan2;
				}

				/*
					* @desc method perkalian digunakan untuk mengalikan bilangan1 dan bilangan2
					* @param $bilangan1 berisi nilai dari bilangan ke 1 atau operand ke 1
					* @param $bilangan2 berisi nilai dari bilangan ke 2 atau operand ke 2
					* @return berupa $bilangan1*$bilangan2 yaitu perkalian $bilangan 1 dan $bilangan 2
				*/
				function perkalian($bilangan1, $bilangan2){
					return $bilangan1*$bilangan2;
				}

				/*
					* @desc method pembagian digunakan untuk membagikan bilangan1 dan bilangan2
					* @param $bilangan1 berisi nilai dari bilangan ke 1 atau operand ke 1
					* @param $bilangan2 berisi nilai dari bilangan ke 2 atau operand ke 2
					* @return berupa $bilangan1/$bilangan2 yaitu pembagian $bilangan 1 dan $bilangan 2
				*/
				function pembagian($bilangan1, $bilangan2){
					return $bilangan1/$bilangan2;
				}
            echo '
            <svg class="Rectangle_8">
            		<rect id="Rectangle_8" rx="4" ry="4" x="0" y="0" width="292" height="33">
            		</rect>
            </svg>
            <svg class="Rectangle_9">
                <rect id="Rectangle_9" rx="4" ry="4" x="0" y="0" width="292" height="33">
                </rect>
            </svg>
            <svg class="Rectangle_10">
                <rect id="Rectangle_10" rx="0" ry="0" x="0" y="0" width="292" height="38">
                </rect>
            </svg>
            <svg class="Rectangle_11">
                <rect id="Rectangle_11" rx="0" ry="0" x="0" y="0" width="292" height="38">
                </rect>
            </svg>
            <svg class="Rectangle_12">
                <rect id="Rectangle_12" rx="0" ry="0" x="0" y="0" width="292" height="38">
                </rect>
            </svg>
            <svg class="Rectangle_13">
                <rect id="Rectangle_13" rx="0" ry="0" x="0" y="0" width="292" height="38">
                </rect>
            </svg>
            <div id="Bilangan_pertama__1000">
                <span>Bilangan pertama : '.$bilangan1.'</span>
            </div>
            <div id="Bilangan_kedua__1000">
                <span>Bilangan kedua : '.$bilangan2.'</span>
            </div>
            <div id="Penambahan__2000">
                <span>Penambahan : '.penjumlahan($bilangan1, $bilangan2).'</span>
            </div>
            <div id="Pengurangan__2000">
                <span>Pengurangan : '.pengurangan($bilangan1, $bilangan2).'</span>
            </div>
            <div id="Perkalian__2000">
                <span>Perkalian : '.perkalian($bilangan1, $bilangan2).'</span>
            </div>
            <div id="Pembagian__2000">
                <span>Pembagian : '.pembagian($bilangan1, $bilangan2).'</span>
            </div>';
		}
	?>
</div>
</body>
</html>